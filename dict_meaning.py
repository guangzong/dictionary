from html.parser import HTMLParser
import requests
import re


class MyHTMLParser(HTMLParser):
    """
            对得到的html提取,词意的文本
    """
    data = ''

    def handle_data(self, data):
        self.data += data
        if self.data == '网络':
            self.data += '  '


def get_one_page(url):
    """
            得到搜索页面文本
    """
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0'
    }
    try:
        response = requests.get(url, headers=headers, timeout=2)
    except Exception as e:
        print(e)
    else:
        response.close()
        if response.status_code == 200:
            return response.text
        return None


def get_meaning(word):
    """
            得到词意以及读音，返回为list
    """
    url = 'https://cn.bing.com/dict/search?q='
    url += word
    html = get_one_page(url)
    # print(html)
    if html is not None:
        meaning = []
        sound = []
        sound += re.findall('<div class="hd_prUS">美&#160;(.*?)</div>', html)
        sound += re.findall('<div class="hd_pr">英&#160;(.*?)</div>', html)
        for i, v in enumerate(sound):
            sound[i] = sound[i].replace('&#230;', 'æ')
            sound[i] = sound[i].replace('&#240;', 'ð')
        if sound:
            meaning.append('美：' + sound[0])
            meaning.append('英：' + sound[1])
        results = re.findall('<li.*?><span class="pos.*?</li>', html)
        parse = MyHTMLParser()
        for i, v in enumerate(results):
            parse.feed(v)
            ans = parse.data
            parse.data = ''
            meaning.append(ans)
        return meaning
