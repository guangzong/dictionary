#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @File  : dictionary1.py
# @Author: Wendell
# @Date  : 11/1/18
# @Desc  :
import gi

gi.require_version('Gtk', "3.0")

from gi.repository import Gtk, Gdk, GObject
from dict_meaning import get_meaning


class dictionary(GObject.GObject):
	__gsignals__ = {'my_hide_signal': (GObject.SIGNAL_RUN_FIRST, None, ())}

	window = Gtk.Window()
	search_entry = Gtk.SearchEntry()
	meaning_view = Gtk.TextView()
	clip = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
	width = 0
	height = 0

	def __init__(self):
		GObject.GObject.__init__(self)
		self.clip.connect('owner-change', self.clipboard_change)
		builder = Gtk.Builder()
		builder.add_from_file('dictionary_ui.glade')
		builder.connect_signals(self)
		self.window = builder.get_object('dictionary')
		self.search_entry = builder.get_object('search_entry')
		self.search_entry.set_text('Input Word')
		self.meaning_view = builder.get_object('meaning_view')
		self.meaning_view.get_buffer().set_text('hello world!')
		# buffer.set_text('hhhhhhhhh',-1)
		# self.meaning_view.set_text('Hello World')
		self.window.connect("destroy", Gtk.main_quit)
		self.window.set_opacity(0.9)
		accel = Gtk.AccelGroup()
		key, mods = Gtk.accelerator_parse('Escape')
		accel.connect(
			key, mods, Gtk.AccelFlags.VISIBLE,
			lambda accel_group, acceleratable, keyval, modifier: self.emit('my_hide_signal')
		)
		self.window.add_accel_group(accel)
		self.search_entry.connect("activate", self.search)
		self.show()


	def search(self, *args):
		search_word = self.search_entry.get_text()
		meaning = get_meaning(search_word)
		if meaning is not None:
			text = ''
			for i in meaning:
				text += i
				text += '\n'
			# self.meaning_view.set_text(text)
			self.meaning_view.get_buffer().set_text(text)
		self.search_entry.set_text(search_word)

	def clipboard_change(self, *args):
		text = self.clip.wait_for_text()
		text.strip()
		self.search_word = text
		self.search_entry.set_text(text)
		self.search()
		self.show()

	def do_my_hide_signal(self):
		self.window.hide()

	def show(self):
		self.window.set_keep_above(True)
		self.window.move(self.window.get_screen().get_width() - self.width,
						 self.window.get_screen().get_height() - self.height)
		self.window.show_all()


if __name__ == '__main__':
	m_dictionary = dictionary()
	Gtk.main()
