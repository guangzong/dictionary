# encoding=utf-8
import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow
import dict_meaning
from dictionary_ui import Ui_MainWindow

class MainWindow(QMainWindow):
	def __init__(self):
		super().__init__()
		clipboard = QApplication.clipboard()
		clipboard.dataChanged.connect(self.clipboard_change)
		self.ui = Ui_MainWindow()
		self.ui.setupUi(self)
		self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint |
							QtCore.Qt.FramelessWindowHint
							# QtCore.Qt.WindowNoState 
							# QtCore.Qt.Tool)
		)
		self.move(QApplication.desktop().width() - self.width(), QApplication.desktop().height() - self.height())
		self.setAttribute(QtCore.Qt.WA_TranslucentBackground)
		self.show()
		QtWidgets.QShortcut(QtGui.QKeySequence("Esc"), self, self.hide)

	
	def search(self):
		word = self.ui.lineEdit.text()
		meaning = dict_meaning.get_meaning(word)
		if meaning is not None:
			text = ''
			for i in meaning:
				text += i
				text += '\n'
			self.ui.label.setText(text)
			self.ui.lineEdit.selectAll()
		else:
			self.ui.lineEdit.selectAll()
	
	def clipboard_change(self):
		clipboard = QApplication.clipboard()
		data = clipboard.mimeData()
		self.ui.lineEdit.setText(data.text())
		self.search()
		self.show()
		

if __name__ == '__main__':
	app = QApplication([])
	window = MainWindow()
	# app.connect(window,window.close,app,app.quit)
	# app.quitOnLastWindowClosed()
	app.exec()
	# sys.exit(0)